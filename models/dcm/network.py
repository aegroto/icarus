import torch
import torch.nn as nn

class DCBased(nn.Module):
    def __init__(self):
        super(DCBased, self).__init__()

        channels = 3
        features = 32
        kernel_size = 3
        padding = kernel_size//2

        raw_head = [
            nn.Conv2d(channels, features, kernel_size, padding=padding)
        ]

        raw_body = [
            nn.LeakyReLU(),
            # nn.Conv2d(features, 48, kernel_size, padding=padding),

            # nn.Linear(256, 256),
            # nn.Linear(256, 256),
            # nn.Conv2d(48, 64, kernel_size, padding=padding),

            nn.LeakyReLU(),
            # nn.Conv2d(48, features, kernel_size, padding=padding),
        ]

        raw_tail = [
            nn.Conv2d(features, channels, kernel_size, padding=padding)
        ]

        self.head = nn.Sequential(*raw_head)
        self.body = nn.Sequential(*raw_body)
        self.tail = nn.Sequential(*raw_tail)

    def forward(self, x):
        x = self.head(x)
        x = self.body(x)
        x = self.tail(x)

        return x