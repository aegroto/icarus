import csv
import webp

import numpy as np

import os

from torch.utils.data import Dataset

class WebPImagesDataset(Dataset):
    def __init__(self, low_quality_images_root_dir, high_quality_images_root_dir, meta_file_path, peek=0):
        self.low_quality_images_root_dir = low_quality_images_root_dir
        self.high_quality_images_root_dir = high_quality_images_root_dir

        meta_reader = csv.reader(open(meta_file_path, 'r'))
        next(meta_reader)

        self.meta = dict()

        row_idx = 0
        for row in meta_reader:
            self.meta[row_idx] = {
                'filename': row[0],
                'width': row[1],
                'height': row[2]
            }

            row_idx += 1
            if peek > 0 and row_idx >= peek:
                break

    def __len__(self):
        return len(self.meta)

    def __getitem__(self, idx):
        image_meta = self.meta[idx]

        low_quality_image_path = os.path.join(self.low_quality_images_root_dir, image_meta['filename'])
        high_quality_image_path = os.path.join(self.high_quality_images_root_dir, image_meta['filename'])

        return {
            "low_quality_image": webp.imread(low_quality_image_path, pilmode="RGB").astype(np.float32),
            "high_quality_image": webp.imread(high_quality_image_path, pilmode="RGB").astype(np.float32),
        }  