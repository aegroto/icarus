import torch
import numpy as np

from pytorch_lightning import LightningModule
from torch import nn

class LossBasedModel(LightningModule):
    def __init__(self, width, height, channels, learning_rate=0.002):
        super().__init__()

        self.dims = (channels, width, height)
        self.learning_rate = learning_rate
        
    def forward(self, x):
        y = self.model(x)
        return y

    def __calculate_batch_loss(self, batch):
        lq_img = batch['low_quality_image']
        hq_img = batch['high_quality_image']

        reconstructed_hq_img = self(lq_img)

        loss = self.loss(hq_img, reconstructed_hq_img)

        # print(f"Output: {reconstructed_hq_img}")
        # print(f"Expected output: {hq_img}")
        # print(f"Loss: {loss}")

        return loss

    def training_step(self, batch, batch_idx):
        loss = self.__calculate_batch_loss(batch)
        return loss

    def validation_step(self, batch, batch_idx):
        loss = self.__calculate_batch_loss(batch)
        return loss

    def test_step(self, batch, batch_idx):
        return self.validation_step(batch, batch_idx)

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters(), lr=self.learning_rate)

    def training_step_end(self, training_loss):
        self.log('training_loss', training_loss.cpu())
        self.log('denormalized_training_loss', training_loss.cpu() * 255.)

    def validation_step_end(self, validation_loss):
        self.log('validation_loss', validation_loss.cpu())
        self.log('denormalized_validation_loss', validation_loss.cpu() * 255.)

    def training_epoch_end(self, training_losses):
        training_losses = [t['loss'].cpu() for t in training_losses]
        self.log('epoch_training_loss', np.mean(training_losses))

    def validation_epoch_end(self, validation_losses):
        validation_losses = [t.cpu() for t in validation_losses]
        self.log('epoch_validation_loss', np.mean(validation_losses))
