from torch import nn

from models.dpir.network_dncnn import DnCNN
from models.loss_based import LossBasedModel

class DnCNNModel(LossBasedModel):
    def __init__(self, width, height, channels, learning_rate=0.002):
        super().__init__(width, height, channels, learning_rate)

        self.loss = nn.MSELoss()
        self.model = DnCNN(in_nc=256, out_nc=256, nc=channels)
        
