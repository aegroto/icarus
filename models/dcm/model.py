from torch import nn
from models.dcm.utils import dcm_preprocessing, dcm_postprocessing
from models.dcm.network import DCBased
from models.loss_based import LossBasedModel

class DCBasedModel(LossBasedModel):
    def __init__(self, width, height, channels, learning_rate=0.002):
        super().__init__(width, height, channels, learning_rate)

        self.loss = nn.L1Loss()
        self.model = DCBased()

        self.__preprocess = dcm_preprocessing
        self.__postprocess = dcm_postprocessing

    def forward(self, x):
        processed_input = self.__preprocess(x)
        unprocessed_output = super().forward(processed_input)
        output = self.__postprocess(unprocessed_output)
        return output
