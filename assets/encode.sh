DATASET=$1
SOURCE=$2
DEST=$3
FORMAT=$4
ENCODER_PARAMS=$5

INPUT_FOLDER="$DATASET/$SOURCE"
OUTPUT_FOLDER="$DATASET/$DEST"

rm -r $OUTPUT_FOLDER
mkdir -p $OUTPUT_FOLDER

for fullfile in $INPUT_FOLDER/* 
do 
	filename=$(basename -- "$fullfile")
	extension="${filename##*.}"
	filename="${filename%.*}"
	output_filename="$OUTPUT_FOLDER/$filename.$FORMAT"

	echo "Encoding '$fullfile' > '$output_filename'..."
	# ffmpeg -i "$fullfile" "$output_filename"; 
	convert "$fullfile" $ENCODER_PARAMS "$output_filename"
done
