from torch import nn

from models.edar.network import EDAR
from models.loss_based import LossBasedModel


class EDARModel(LossBasedModel):
    def __init__(self, width, height, channels, learning_rate=0.002):
        super().__init__(width, height, channels, learning_rate)

        self.loss = nn.MSELoss()
        self.model = EDAR()

    def forward(self, x):
        return self.__postprocess(super().forward(self.__preprocess(x)))

    def __preprocess(self, tensor):
        batch_size = tensor.size(0)
        tensor = tensor.reshape((batch_size, 3, 256, 256))
        tensor /= 255.

        return tensor

    def __postprocess(self, tensor):
        tensor *= 255.
        batch_size = tensor.size(0)
        tensor = tensor.reshape((batch_size, 256, 256, 3))

        return tensor
