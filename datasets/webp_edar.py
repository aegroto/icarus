import webp

import numpy as np

import os

from torch.utils.data import Dataset

from datasets.webp import WebPImagesDataset


# Normalize each pixel between the interval [0, 1] and reshapes the images
class EDARWebPImagesDataset(WebPImagesDataset):
    def __init__(
        self,
        low_quality_images_root_dir,
        high_quality_images_root_dir,
        meta_file_path,
        peek=0,
    ):
        super(EDARWebPImagesDataset, self).__init__(
            low_quality_images_root_dir,
            high_quality_images_root_dir,
            meta_file_path,
            peek,
        )

    def __getitem__(self, idx):
        item = super(EDARWebPImagesDataset, self).__getitem__(idx)

        item['low_quality_image'] = self.__normalize(item['low_quality_image'])
        item['high_quality_image'] = self.__normalize(item['high_quality_image'])

        return item

    def __normalize(self, tensor):
        tensor = tensor.reshape((3, 256, 256))
        tensor /= 255.

        return tensor
