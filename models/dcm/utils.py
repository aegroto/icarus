def dcm_preprocessing(tensor):
    processed_tensor = tensor.clone()

    batch_size = processed_tensor.size(0)
    processed_tensor = processed_tensor.reshape((batch_size, 3, 256, 256))
    processed_tensor /= 255.

    return processed_tensor

def dcm_postprocessing(tensor):
    processed_tensor = tensor.clone()

    batch_size = processed_tensor.size(0)
    processed_tensor = processed_tensor.reshape((batch_size, 256, 256, 3))
    processed_tensor *= 255.

    return processed_tensor