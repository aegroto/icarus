import os

import torch
from pytorch_lightning import LightningModule, Trainer
from torch import nn
from torch.nn import functional as F
from torch.utils.data import DataLoader, random_split
from torchmetrics import Accuracy
from torchvision import transforms
from torchvision.datasets import MNIST

from datasets.webp import WebPImagesDataset
from models.dcm.model import DCBasedModel
from models.dpir.model_dncnn import DnCNNModel
from models.dpir.model_unet import UNetModel
from models.dpir.network_unet import UNet
from models.edar.model import EDARModel
from models.linear import LinearModel

from pytorch_lightning import loggers as pl_loggers

def main():
    dataset_id = "ilsvrc"
    source_flavour = "256crop_webp_q10"
    dest_flavour = "256crop_webp_q90"
    meta_file = "256crop_webp_q10.csv"
    peek = 0

    epochs = 1000
    batch_size = 32

    print("Loading dataset...")

    dataset = WebPImagesDataset(
        low_quality_images_root_dir=f"assets/{dataset_id}/{source_flavour}", 
        high_quality_images_root_dir=f"assets/{dataset_id}/{dest_flavour}",
        meta_file_path=f"assets/{dataset_id}/{meta_file}",
        peek=peek
    )

    train_elements = int(len(dataset) * 0.8)
    validation_elements = int(len(dataset) * 0.1)
    test_elements = len(dataset) - train_elements - validation_elements

    train_dataset, validation_dataset, test_dataset = random_split(dataset, lengths=[
        train_elements,
        validation_elements,
        test_elements,
    ], generator=torch.Generator().manual_seed(42))

    print("Loading the model...")

    model_id = "dcm"
    model = DCBasedModel(256, 256, 3, 0.02)

    model_path = f"assets/trained_models/{model_id}_{dataset_id}_{source_flavour}__to__{dest_flavour}.pth"
    if os.path.exists(model_path):
        print(f"Loading existing model {model_path}...")
        model.load_state_dict(torch.load(model_path))
    else:
        print(f"No pre-trained model found at {model_path}, training from scratch")

    model.train()

    print("Training...")

    trainer = Trainer(
        gpus=1,
        max_epochs=epochs,
        progress_bar_refresh_rate=5,
        log_every_n_steps=1
    )

    trainer.fit(model, 
        train_dataloader=DataLoader(train_dataset, batch_size, num_workers=8),
        val_dataloaders=DataLoader(validation_dataset, batch_size, num_workers=8)
    )

    print("Testing and evaluation...")

    trainer.test(model, DataLoader(test_dataset, batch_size, num_workers=8))

    model.eval()

    print("Saving the model...")

    torch.save(model.state_dict(), model_path)

if __name__ == "__main__":
    main()
