DATASET=$1
SOURCE=$2

INPUT_FOLDER="$DATASET/$SOURCE"
FILE_PATH="$DATASET/$SOURCE.csv"

> $FILE_PATH

echo "filename,width,height" >> $FILE_PATH

for fullfile in $INPUT_FOLDER/*
do 
	identify -format "%f,%w,%h\n" "$fullfile" >> $FILE_PATH
done
