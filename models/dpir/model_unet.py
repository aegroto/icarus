import torch
from torchvision import transforms

from pytorch_lightning import LightningModule
from torch import nn

from models.dpir.network_unet import UNet

class UNetModel(LightningModule):
    def __init__(self, width, height, channels):
        super().__init__()

        self.dims = (channels, width, height)

        self.model = UNet(in_nc=256, out_nc=256, nc=[16, 32, 64, 128], nb=2)
        
    def forward(self, x):
        x = x.select(3, 0)

        x = self.model(x)
        return nn.functional.log_softmax(x, dim=1)

    def __calculate_batch_mse(self, batch):
        lq_img = batch['low_quality_image']
        hq_img = batch['high_quality_image']

        reconstructed_hq_img = self(lq_img)

        return torch.mean((hq_img - reconstructed_hq_img) ** 2)

    def training_step(self, batch, batch_nb):
        return self.__calculate_batch_mse(batch)

    def validation_step(self, batch, batch_idx):
        loss = self.__calculate_batch_mse(batch)
        self.log("val_loss", loss, prog_bar=True)
        return loss

    def test_step(self, batch, batch_idx):
        # Here we just reuse the validation_step for testing
        return self.validation_step(batch, batch_idx)

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters(), lr=0.02)