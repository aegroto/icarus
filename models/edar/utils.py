def edar_input_preprocessing(tensor):
    tensor = tensor.reshape((1, 3, 256, 256))
    tensor /= 255.0

    return tensor

def edar_output_preprocessing(tensor):
    tensor *= 255.0
    tensor = tensor.reshape((1, 256, 256, 3))

    return tensor