import sys
import webp
import torch
import numpy as np
from models.dcm.utils import dcm_preprocessing, dcm_postprocessing
from models.dcm.model import DCBasedModel

from pytorch_msssim import ssim, ms_ssim

from torch import nn

def main():
    model_path = sys.argv[1]
    input_image_path = sys.argv[2]
    output_image_path = sys.argv[3]
    expected_output_image_path = sys.argv[4]

    print(f"Loading model {model_path}...")

    model = DCBasedModel(256, 256, 3, 0.02)
    preprocessing = dcm_preprocessing
    postprocessing = dcm_postprocessing
    model.load_state_dict(torch.load(model_path))
    model.eval()

    print(f"Loading image {input_image_path}...")
    input_image = webp.imread(input_image_path, pilmode="RGB").astype(np.float32)

    print("Elaboration...")
    input_image_tensor = torch.from_numpy(input_image)
    input_image_tensor = input_image_tensor[None, :]
    output_image_tensor = postprocessing(model(preprocessing(input_image_tensor)))

    print(f"Writing output image {output_image_path}...")
    output_image = output_image_tensor.squeeze().detach().numpy().astype(np.uint8)
    webp.imwrite(output_image_path, output_image, pilmode="RGB")

    expected_output_image = webp.imread(expected_output_image_path, pilmode="RGB").astype(np.float32)
    expected_output_image_tensor = torch.from_numpy(expected_output_image)
    expected_output_image_tensor = expected_output_image_tensor[None, :]

    mse = nn.MSELoss()
    mae = nn.L1Loss()

    input_image_tensor = input_image_tensor.reshape((1, 3, 256, 256))
    output_image_tensor = output_image_tensor.reshape((1, 3, 256, 256))
    expected_output_image_tensor = expected_output_image_tensor.reshape((1, 3, 256, 256))

    print(f"Input: ({input_image_tensor.shape}) {input_image_tensor}")
    print(f"Output: ({output_image_tensor.shape}) {output_image_tensor}")
    print(f"Expected output: ({expected_output_image_tensor.shape}) {expected_output_image_tensor}")

    print(f"Input/Output MSE: {mse(input_image_tensor, output_image_tensor)}")
    print(f"Input/Expected output MSE: {mse(input_image_tensor, expected_output_image_tensor)}")
    print(f"Output/Expected output MSE: {mse(expected_output_image_tensor, output_image_tensor)}")

    print(f"Input/Output MAE: {mae(input_image_tensor, output_image_tensor)}")
    print(f"Input/Expected output MAE: {mae(input_image_tensor, expected_output_image_tensor)}")
    print(f"Output/Expected output MAE: {mae(expected_output_image_tensor, output_image_tensor)}")

    print(f"Input/Output SSIM: {ssim(input_image_tensor, output_image_tensor)}")
    print(f"Input/Expected output SSIM: {ssim(input_image_tensor, expected_output_image_tensor)}")
    print(f"Output/Expected output SSIM: {ssim(expected_output_image_tensor, output_image_tensor)}")

    print(f"Input/Output MS-SSIM: {ms_ssim(input_image_tensor, output_image_tensor)}")
    print(f"Input/Expected output MS-SSIM: {ms_ssim(input_image_tensor, expected_output_image_tensor)}")
    print(f"Output/Expected output MS-SSIM: {ms_ssim(expected_output_image_tensor, output_image_tensor)}")

if __name__ == "__main__":
    main()
